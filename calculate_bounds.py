import numpy as np


id = np.array([[1, 0], [0, 1]])
sx = np.array([[0, 1], [1, 0]])
sy = np.array([[0, -1j], [1j, 0]])
sz = np.array([[1, 0], [0, -1]])
sigma = [sx, sy, sz]


def rho(r):
    return (id + sum([r[k] * sigma[k] for k in range(3)])) / 2


n = 9

list_of_states = []

list_of_effects = []

for i in range(n):
    list_of_states.append(
        rho([np.cos(i * 2 * np.pi / n), 0, np.sin(i * 2 * np.pi / n)])
    )
    list_of_effects.append(
        2 * rho([-np.cos(i * 2 * np.pi / n), 0, -np.sin(i * 2 * np.pi / n)]) / n
    )

# for state in list_of_states:
#    print(state)

# print(sum(list_of_effects))


communication_matrix = []

for state in list_of_states:
    row_probabilities = []
    row_string = ""
    for effect in list_of_effects:
        row_probabilities.append(np.trace(state @ effect))
        row_string += str(np.round(np.trace(state @ effect).real, 6)) + "\t"
    communication_matrix.append(row_probabilities)
    print(row_string)

communication_matrix = np.array(communication_matrix)
# print(communication_matrix)
