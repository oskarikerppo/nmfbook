syms a b c
A_7 = [0   a   b    c    c    b    a
       a    0    a   b    c    c    b 
       b     a    0    a   b    c    c
       c     b     a    0    a   b    c
       c     c     b     a    0    a   b 
       b     c     c     b     a    0    a
       a    b     c     c     b     a    0]

[W, H, e, t] = exactNMFheur(A_7, 6)
W
H
W*H
A_7 - W*H
